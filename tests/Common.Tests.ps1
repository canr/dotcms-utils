﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$parent = Split-Path -Parent "$here"
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path) -replace '\.Tests\.', '.'
. "$parent\$sut"

Describe "WaitUntilLogEntryMatchesRegex" {
    It "tails the given log until a new line matches the given regex." {
        $targetLog = "$here\fixtures\test.log"
        $date = Get-Date
        $targetStr = "Test: $date"

        try {
            # Start job with wait
            $waitJob = Start-Job -ArgumentList "$targetLog","$targetStr","$parent","$sut" -ScriptBlock {
                Param ($targetLog, $targetStr, $parent, $sut)
                . "$parent\$sut"

                WaitUntilLogEntryMatchesRegex `
                    -logFilePath "$targetLog" `
                    -targetRegex "$targetStr"
            } 
            sleep 5

            # Append target string to log
            Add-Content "$targetLog" "$targetStr"
            sleep 1

            # Has the wait job completed?
            Add-Content "$targetLog" $waitJob.State
            $waitJob.State | Should Be 'Completed'
        }
        catch {
            $true | Should Be $false
        }
    }
}
