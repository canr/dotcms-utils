using System.Xml;
using NGitLab;

namespace DotcmsUtils
{
    /// <summary>
    /// Creates repository classes implementing <see cref="DotcmsUtils.IRepository" />.
    public class RepositoryFactory
    {
        /// <summary>
        /// Creates a repository class implementing <see cref="DotcmsUtils.IRepository" /> defined by the given XML.
        /// <param name="repositoryReader">An <see cref="System.Xml.XmlReader" /> which is set to read XML from the plugins.xml file that defines a repository to be created.</param>
        /// <returns>A <see cref="DotcmsUtils.IRepository" /> representing the XML-defined repository.</returns>
        public static IRepository CreateRepository ( XmlReader repositoryReader )
        {
            string repositoryType = repositoryReader["type"];
            switch ( repositoryType )
            {
                case "Gitlab":
                    // Read
                    string baseUrl = repositoryReader["baseUrl"];
                    int apiVersion = System.Convert.ToInt32( repositoryReader["apiVersion"] );
                    string apiToken = repositoryReader["apiToken"];
                    GitLabClient gitlabApi = GitLabClient.Connect(baseUrl, apiToken);

                    // Instantiate by ID or pathWithNamespace
                    int id = System.Convert.ToInt32( repositoryReader["id"] );
                    if ( id != 0 )
                    {
                        return new GitlabRepository(id, gitlabApi, baseUrl);
                    }
                    else
                    {
                        string pathWithNamespace = repositoryReader["pathWithNamespace"];
                        return new GitlabRepository(pathWithNamespace, gitlabApi, baseUrl);
                    }


                default:
                    throw new System.Exception(string.Format("Unable to create repository of type {0}.", repositoryType));
            }
        }
    }
}
