﻿<#
.SYNOPSIS
Redeploys plugins in the given dotCMS instance.

.DESCRIPTION
Shuts down dotCMS, undeploys old plugin files, deploys current plugin files, and regenerates the service wrapper.

.NOTES
Will require admin privileges on most systems.

.PARAMETER dotCmsDir
Path to the root directory of the dotCMS instance to be wrapped in the service wrapper.

.PARAMETER yajswDir
Path to the root directory of the YAJSW service wrapper.

.EXAMPLE
RedeployPlugins.ps1 -dotCmsDir "E:\dotCMS\dotcms_3.3" -yajswDir "C:\yajsw"
#>
Param(
    [Parameter(Mandatory=$true, Position=1)] [string] $dotCmsDir,
    [Parameter(Mandatory=$true, Position=2)] [string] $yajswDir
)

# Includes
$here = Split-Path -parent $MyInvocation.MyCommand.Definition
. "$here\Common.ps1"

# Admin check
CheckContinueIfNotAdmin

# Save current directory
$initPwd = pwd

# Shut down dotCMS service if it exists
$serviceWasRunningOnScriptStartup = $false
If (Get-Service dotCMS -ErrorAction SilentlyContinue) {
    $dotCmsServiceStatus = Get-Service dotCMS | Select -ExpandProperty 'Status'
    If ("$dotCmsServiceStatus" -ne 'Stopped') {
        $serviceWasRunningOnScriptStartup = $true
        echo 'Stopping dotCMS service.'
        Stop-Service dotCMS
    }
}

# Undeploy
cmd /c "$dotCmsDir\bin\undeploy-plugins.bat" 2>&1 | Tee-Object -variable undeployOut
If ($undeployOut -Like '*BUILD FAILED*') {
    $host.ui.WriteErrorLine('Unable to undeploy plugins.')
    exit 1
}

# Deploy
cmd /c "$dotCmsDir\bin\deploy-plugins.bat" 2>&1 | Tee-Object -variable deployOut
If ($deployOut -Like '*BUILD FAILED*') {
    $host.ui.WriteErrorLine('Unable to deploy plugins.')
    exit 1
}

# Service wrapper
echo 'Generating YAJSW config for dotCMS.'
cd "$here"
.\GenerateServiceWrapper.ps1 -dotCmsDir "$dotCmsDir" -yajswDir "$yajswDir"

# Restore dotCMS service
If ( -not $serviceWasRunningOnScriptStartup ) {
    $userWantsServiceStartedResponse = Read-Host -prompt 'Start dotCMS service? (y/N)'
    $userWantsServiceStarted = ( $userWantsServiceStartedResponse -eq 'y' -or $userWantsServiceStartedResponse -eq 'Y' )
}
If ($serviceWasRunningOnScriptStartup -or $userWantsServiceStarted) {
    echo 'Starting dotCMS service.'
    Start-Service dotCMS

    echo 'Waiting for dotCMS to start...'
    WaitForDotCmsStartup "$dotCmsDir"

    echo 'dotCMS started.'
}

# Restore working directory
cd "$initPwd"
