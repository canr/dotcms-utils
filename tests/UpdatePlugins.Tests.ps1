$here = Split-Path -Parent $MyInvocation.MyCommand.Path
$parent = Split-Path -Parent "$here"
$sut = (Split-Path -Leaf $MyInvocation.MyCommand.Path) -replace '\.Tests\.', '.'
. "$parent\$sut"

Describe "GetPluginInstalledVersion" {
    It "Returns the version string for the given plugin directory." {
        $True | Should Be $False
    }
}

Describe "GetGitlabProjectId" {
    It "Returns the Gitlab project ID of the given project." {
        $True | Should Be $False
    }
}

Describe "GetGitlabProjectTags" {
    It "Returns a list of the given Gitlab project's tags." {
        $True | Should Be $False
    }
}

Describe "PluginHasUpdateAvailable" {
    It "Returns whether or not the given plugin has updates available." {
        $True | Should Be $False
    }
}

Describe "InstallPluginVersion" {
    Context "When an update is available for the given plugin" {
        It "Installs the given version of the given plugin in the given dotCMS instance." {
            $True | Should Be $False
        }

        It "Archives the old version of the given plugin in the given archive directory." {
            $True | Should Be $False
        }
    }

    Context "When no update is available for the given plugin" {
        It "Throws an error." {
            $True | Should Be $False
        }
    }
}
