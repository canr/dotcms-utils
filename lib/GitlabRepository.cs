using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using NGitLab;

namespace DotcmsUtils
{
    /// <summary>
    /// A dotCMS plugin repository hosted on Gitlab.</summary>
    /// <seealso cref="DotcmsUtils.IRepository" />
    public class GitlabRepository : IRepository
    {
        private GitLabClient Client;
        private NGitLab.Models.Project Project;
        private IRepositoryClient Repo;
        private readonly string BaseUrl;

        public int Id                   { get { return this.Project.Id; } }
        public string PathWithNamespace { get { return this.Project.PathWithNamespace; } }
        public string Name              { get { return this.Project.Name; } }
        public string Path              { get { return this.Project.Path; } }
        public string Namespace         { get { return this.Project.Namespace.Name; } }
        public string Type              { get { return "Gitlab"; } }


        /// <summary>
        /// Instantiates a Gitlab repository given the project's ID number, a client object, and the Gitlab instance's URL.</summary>
        /// <param name="id">The ID number of the Gitlab project whose repository is represented by this instance.</param>
        /// <param name="client">The <see cref="NGitLab.GitLabClient" /> used to connect to the Gitlab instance where this repository is hosted.</param>
        /// <param name="baseUrl">The URL of the Gitlab instance where this repository is hosted.</param>
        public GitlabRepository ( int id, GitLabClient client, string baseUrl )
        {
            this.Client = client;
            this.Project = this.Client.Projects[ id ];
            this.Repo = this.Client.GetRepository( this.Id );
            this.BaseUrl = baseUrl;
        }

        /// <summary>
        /// Instantiates a Gitlab repository given the project's path with namespace, a client object, and the Gitlab instance's URL.</summary>
        /// <param name="pathWithNamespace">The namespaced path of the Gitlab project whose repository is represented by this instance.</param>
        /// <param name="client">The <see cref="NGitLab.GitLabClient" /> used to connect to the Gitlab instance where this repository is hosted.</param>
        /// <param name="baseUrl">The URL of the Gitlab instance where this repository is hosted.</param>
        public GitlabRepository ( string pathWithNamespace, GitLabClient client, string baseUrl )
        {
            this.Client = client;
            this.Project = this.Client.Projects.Accessible
                .Where( project => project.PathWithNamespace == pathWithNamespace )
                .Single();
            this.Repo = this.Client.GetRepository( this.Id );
            this.BaseUrl = baseUrl;
        }

        public List<Version> GetVersions ()
        {
            return this.Repo.Tags.Select( tag => new Version(tag.Name) ).ToList();
        }

        public Version GetLatestVersion ()
        {
            List<Version> versions = this.GetVersions();
            versions.Sort();
            versions.Reverse();

            return versions.First();
        }

        public void DownloadVersion ( Version version, string destinationDirPath )
        {
            // Download
            string downloadUrl = this.BaseUrl + "/" + this.PathWithNamespace + "/repository/archive.zip?reg=" + version.ToString();
            string destinationFilePath = Directory.GetParent(destinationDirPath) + "\\" + this.Name + "-" + version.ToString() + ".zip";
            WebClient webClient = new WebClient();
            webClient.DownloadFile( downloadUrl, destinationFilePath );

            // Clean up previously extracted versions
            List<string> previouslyExtractedDirPaths = Directory.GetDirectories( Directory.GetParent(destinationDirPath).ToString(), this.Name + "-master-*" ).ToList();
            foreach ( string previouslyExtractedDirPath in previouslyExtractedDirPaths )
            {
                Directory.Delete( previouslyExtractedDirPath, true );
            }
            
            // Extract new version
            string tempExtractionPath = destinationDirPath + "-temp";
            ZipFile.ExtractToDirectory( destinationFilePath , tempExtractionPath );
            string extractedDirPath = Directory.GetDirectories( tempExtractionPath, this.Name + "-master-*" )
                .ToList()
                .Single();

            // Rename extracted directory
            Directory.Move( extractedDirPath, destinationDirPath );
        }

    }
}
