using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace DotcmsUtils
{
    /// <summary>
    /// Represents a dotCMS instance.</summary>
    public class DotcmsInstance
    {
        public static string PluginsSubdirectory        = "\\plugins";
        public static string PluginDownloadsSubdirectory = "\\plugin-downloads";
        public static string PluginArchivesSubdirectory  = "\\plugins-archives";
        public static string PluginsFile                = "\\plugins\\plugins.xml";

        public string PluginsSubdirectoryPath           { get { return this.Path + DotcmsInstance.PluginsSubdirectory; } }
        public string PluginDownloadsSubdirectoryPath    { get { return this.Path + DotcmsInstance.PluginDownloadsSubdirectory; } }
        public string PluginArchivesSubdirectoryPath       { get { return this.Path + DotcmsInstance.PluginArchivesSubdirectory; } }
        public string PluginsFilePath                   { get { return this.Path + DotcmsInstance.PluginsFile; } }

        public string Path;
        public List<DotcmsPlugin> Plugins;


        /// <summary>
        /// Constructs a new DotcmsInstance object representing the dotCMS instance at the given path.</summary>
        /// <param name="path">The path at which the dotCMS instance is installed.</param>
        public DotcmsInstance ( string path )
        {
            this.Path = path;
            this.Plugins = this.CreatePlugins();
        }

        /// <summary>
        /// Instantiates all plugins described in the dotCMS instance's plugins.xml file.</summary>
        /// <returns>A list of DotcmsPlugin objects described in the plugins.xml file.</summary>
        private List<DotcmsPlugin> CreatePlugins ()
        {
            List<DotcmsPlugin> plugins = new List<DotcmsPlugin>();

            using ( XmlReader reader = XmlReader.Create( this.PluginsFilePath ) )
            {
                while ( reader.ReadToFollowing("plugin") )
                {
                    string name = reader["name"];
                    string path = this.PluginsSubdirectoryPath + "/" + name;
                    bool isStatic = reader["static"] == "true";
                    string description = reader["description"];

                    XmlReader repositoryReader = reader.ReadSubtree();
                    repositoryReader.ReadToFollowing("repository");
                    IRepository repository = RepositoryFactory.CreateRepository( repositoryReader );
                    
                    plugins.Add(new DotcmsPlugin(this, name, path, isStatic, repository, description));
                }
            }

            return plugins;
        }

        /// <summary>
        /// Updates the plugins installed in this dotCMS instance to their latest versions.</summary>
        public void UpdatePlugins ()
        {
            bool pluginUpdated = false;

            try
            {
                this.CreateDownloadsSubdir();

                foreach (DotcmsPlugin plugin in this.Plugins)
                {
                    if ( ! plugin.IsInstalled )
                    {
                        plugin.Install();
                        pluginUpdated = true;
                    }
                    else if ( plugin.HasUpdateAvailable() )
                    {
                        plugin.Update();
                        pluginUpdated = true;
                    }
                }

                if ( ! pluginUpdated )
                {
                    System.Console.WriteLine("All plugins are up to date.");
                }
            }
            finally
            {
                this.DeleteDownloadsSubdir();
            }

        }

        /// <summary>
        /// Creates a temporary subdirectory into which plugin files are downloaded.</summary>
        private void CreateDownloadsSubdir ()
        {
            this.DeleteDownloadsSubdir();
            Directory.CreateDirectory( this.PluginDownloadsSubdirectoryPath );
        }

        /// <summary>
        /// Deletes the temporary subdirectory into which plugin files are downloaded.</summary>
        private void DeleteDownloadsSubdir ()
        {
            if ( Directory.Exists( this.PluginDownloadsSubdirectoryPath ) )
            {
                Directory.Delete( this.PluginDownloadsSubdirectoryPath, true );
            }
        }

    }
}
