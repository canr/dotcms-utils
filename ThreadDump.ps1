<#
.SYNOPSIS
Dumps the JVM threads for the current dotCMS process.

.NOTES
Will require admin privileges on most systems.

.PARAMETER outfile
Path to the file to which the thread dump contents should be written. If not
set, output will be displayed on stdout.

.EXAMPLE
.\ThreadDump.ps1 -outfile "C:\Users\admin-slenk\Desktop\dotCMS Thread Dump $(Get-Date -f 'yyyy-MM-dd HH_mm_ss')"
#>
Param(
    [Parameter(Mandatory=$false, Position=1)] [string] $outfile
)

# Includes
$here = Split-Path -parent $MyInvocation.MyCommand.Definition
. "$here\Common.ps1"

# Admin check
CheckContinueIfNotAdmin


# Get dotCMS PID
$java_processes = Get-Process "java" | Sort-Object -Property Id
if ($java_processes.length -eq 0) {
    Write-Error "No JVM processes found. Is the dotCMS service running?"
    Exit
}

# Run stack trace
If ($outfile -ne "") {
    # Clear output file
    "" | Out-File -filePath "$outfile" -force

    # Write stack traces to output file
    $java_processes | Foreach-Object {
        jcmd.exe "$($_.Id)" Thread.print `
        | Out-File -filePath "$outfile" -append
    }
} Else {
    $java_processes | Foreach-Object {
        jcmd.exe "$($_.Id)" Thread.print
    }
}