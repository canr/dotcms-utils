<#
.SYNOPSIS
Updates plugins in the given dotCMS instance.

.DESCRIPTION
Fetches the current version of each plugin as necessary, ports configuration files, archives old plugin versions, and moves new plugin versions into place.

.NOTES
Will require admin privileges on most systems.

.PARAMETER dotcmsDir
Path to the root directory of the dotCMS instance to be wrapped in the service wrapper.

.EXAMPLE
UpdatePlugins.ps1 -dotCmsDir "E:\dotCMS\dotcms_3.3"
#>
Param(
    [Parameter(Mandatory=$True, Position=1)] [string] $dotcmsDir,
    [string] $pluginArchiveDir = ""
)

# Includes
$here = Split-Path -parent $MyInvocation.MyCommand.Definition
. "$here\Common.ps1"

# Load libraries
$dotcmsUtilsLibs = Get-ChildItem -Recurse -Path "$here\lib" | Where { $_.Name.EndsWith(".cs") } | Foreach-Object { $_.FullName }
$nGitlabLibs = Get-ChildItem -Recurse -Path "$here\third_party\NGitLab\NGitLab\NGitLab" | Where { $_.Name.EndsWith(".cs") } | Foreach-Object { $_.FullName }
$libs = $dotcmsUtilsLibs + $nGitlabLibs

$dotcmsUtilsAssemblies = "System","System.Xml","System.IO.Compression.FileSystem"
$nGitlabAssemblies = "System.ComponentModel.DataAnnotations","System.Core","Microsoft.CSharp","System.Data","System.Runtime.Serialization"
$assemblies = $dotcmsUtilsAssemblies + $nGitlabAssemblies

Add-Type -Path $libs -ReferencedAssemblies $assemblies


# Admin check
CheckContinueIfNotAdmin

$dotcms = New-Object -Type DotcmsUtils.DotcmsInstance -ArgumentList "$dotcmsDir"
$dotcms.UpdatePlugins()
