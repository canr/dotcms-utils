using System;
using System.Collections.Generic;

namespace DotcmsUtils
{
    /// <summary>
    /// Defines a plugin repository's requisite functions.</summary>
    public interface IRepository
    {
        /// <value>Specifies the type of code repository.</value>
        string Type { get; }


        /// <summary>
        /// Lists all plugin versions in the code repository.</summary>
        /// <returns>A list of project <see cref="System.Version" /> available.</returns>
        List<Version> GetVersions ();

        /// <summary>
        /// Gets the latest <see cref="System.Version" /> of the project available.</summary>
        /// <returns>The latest <see cref="System.Version" /> of the project available.</returns>
        Version GetLatestVersion ();

        /// <summary>
        /// Downloads the project's release archive at the given version to the given destination.</summary>
        /// <param name="version">The version of the project to be downloaded.</param>
        /// <param name="destinationPath">The path to which the given version's release archive will be downloaded.</param>
        void DownloadVersion ( Version version, string destinationPath );
    }
}
