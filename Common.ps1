﻿<#
.SYNOPSIS
A collection of common functions to be used in other utilities in this package.
#>

Function WaitUntilLogEntryMatchesRegex {
    <#
    .SYNOPSIS
    Blocks the script until a line matching the given regular expression is found in the given log file.

    .PARAMETER logFilePath
    The filepath of the file which is to be monitored for the presence of the given regular expression.

    .PARAMETER targetRegex
    The regular expression to be found in the given log file.

    .EXAMPLE
    WaitUntilLogEntryMatchesRegex -logFilePath "~\test.log" -targetRegex '.*\[INFO\].*'
    #>
    Param(
        [parameter(Mandatory=$true)] [String] $logFilePath,
        [parameter(Mandatory=$true)] [String] $targetRegex
    )

    If ( -not (Test-Path "$logFilePath") ) {
        throw [System.IO.FileNotFoundException] "Could not find file: $logFilePath"
    }

    while ($true) {
        Get-Content "$logFilePath" -Tail 0 -Wait `
        | Select-String -pattern "$targetRegex" `
        | %{ Write-Host $_; break }
    }
}

Function WaitForDotCmsStartup {
    <#
    .SYNOPSIS
    Blocks the script until the given dotCMS instance has successfully started.

    .DESCRIPTION
    This function blocks execution until the Tomcat Catalina log file for today's date has a line containing 'Startup in' appended.

    .PARAMETER dotCmsDir
    The root directory of the dotCMS instance to be monitored for startup.

    .EXAMPLE
    WaitForDotComsStartup "E:\dotCMS\dotcms_3.3"
    #>
    Param( [parameter(Mandatory=$true)] [String] $dotCmsDir )

    $currentDate = Get-Date -Format 'yyyy-MM-dd'
    $logFilePath = "$dotCmsDir\dotserver\tomcat-8.0.18\logs\catalina.$currentDate.log"

    If ( -not (Test-Path "$logFilePath") ) {
        New-Item "$logFilePath" -type file
    }

    WaitUntilLogEntryMatchesRegex `
        -logFilePath  "$logFilePath" `
        -targetRegex '^.*Server startup in \d+ ms$'
}

Function IsCurrentUserAdmin {
    <#
    .SYNOPSIS
    Determines whether the current user has Administrator privileges.

    .OUTPUTS
    System.Boolean. Whether the current user has Administrator privileges.

    .LINK
    https://msdn.microsoft.com/en-us/library/46ks97y7(v=vs.110).aspx
    #>

    ([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]'Administrator')
    
}

Function CheckContinueIfNotAdmin {
    <#
    .SYNOPSIS
    If the user is not an admin, warns and prompts for whether the user wants to proceed.
    #>

    If ( -not (IsCurrentUserAdmin) ) {
        Write-Warning 'You are not an Administrator. This script is meant to be run as an Administrator.'
        $cont = Read-Host -Prompt 'Continue anyway? (y/N)'
        If ( $cont -ne 'y' -and $cont -ne 'Y' ) {
            exit 1
        }
    }
}

