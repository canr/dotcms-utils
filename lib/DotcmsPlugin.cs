using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;


namespace DotcmsUtils
{
    /// <summary>
    /// Represents a dotCMS plugin.</summary>
    public class DotcmsPlugin
    {
        public string Name;
        public string Path;
        public bool IsStatic;
        public bool IsInstalled;
        public string Description;
        public static string ManifestFile = "MANIFEST.MF";

        private DotcmsInstance Dotcms;
        private IRepository Repository;


        /// <summary>
        /// Creates a dotCMS plugin.
        /// <param name="dotcms" cref="DotcmsUtils.DotcmsInstance">The DotcmsInstance to which this plugin belongs.</param>
        /// <param name="name">The name of this plugin.</param>
        /// <param name="path">The filesystem path where this plugin is installed.</param>
        /// <param name="isStatic">Whether or not this plugin is a static plugin.</param>
        /// <param name="repository" cref="DotcmsUtils.IRepository">The code repository where this plugin's code is stored.</param>
        public DotcmsPlugin (DotcmsInstance dotcms, string name, string path, bool isStatic, IRepository repository, string description = "")
        {
            this.Dotcms = dotcms;
            this.Name = name;
            this.Path = path;
            this.IsInstalled = Directory.Exists( this.Path );
            this.IsStatic = isStatic;
            this.Repository = repository;
            this.Description = description;
        }

        /// <summary>
        /// Reads the currently installed version of this plugin from its manifest file.</summary>
        /// <returns>A Version object representing this plugin's current semantic version.</returns>
        public Version GetInstalledVersion ()
        {
            try
            {
                string versionStr = File.ReadLines( this.Path + "\\" + DotcmsPlugin.ManifestFile )
                    .First( line => line.Contains("Plugin-Version: ") )
                    .Split(':')[1];

                return new Version(versionStr);
            }
            catch (FileNotFoundException e)
            {
                System.Console.Error.WriteLine(string.Format("Unable to find MANIFEST.MF file for plugin {0}. Are you sure this is a valid dotCMS plugin?", this.Name));
                throw e;
            }
        }

        /// <summary>
        /// Downloads and installs the specified plugin version.</summary>
        /// <param name="version">The version of this plugin to be installed.</param>
        public void Install (Version version)
        {
            // TODO: Eliminate redundancy with Update()


            System.Console.WriteLine( string.Format( "Installing plugin {0} version {1}.", this.Name, this.GetLatestVersion() ) );

            // Download and extract new version
            string extractedDirPath = this.Dotcms.PluginDownloadsSubdirectoryPath + "\\" + this.Name + "-" + this.Repository.GetLatestVersion().ToString();
            this.Repository.DownloadVersion( this.Repository.GetLatestVersion(), extractedDirPath );

            // Move extracted version
            Directory.Move( extractedDirPath, this.Path );
        }

        /// <summary>
        /// Downloads and installs the latest plugin version.</summary>
        public void Install ()
        {
            this.Install(this.GetLatestVersion());
        }

        /// <summary>
        /// Checks the source code repository to determine the latest version of this plugin.</summary>
        public Version GetLatestVersion ()
        {
            return this.Repository.GetLatestVersion();
        }

        /// <summary>
        /// Checks the latest verision and current installed version to determine whether there is a more recent version of this plugin available.</summary>
        public bool HasUpdateAvailable ()
        {
            return this.GetLatestVersion() > this.GetInstalledVersion();
        }

        /// <summary>
        /// Downloads and installs the latest version of this plugin.</summary>
        public void Update ()
        {
            System.Console.WriteLine( string.Format( "Updating plugin {0} from version {1} to version {2}.", this.Name, this.GetInstalledVersion(), this.GetLatestVersion() ) );

            // Download and extract new version
            string extractedDirPath = this.Dotcms.PluginDownloadsSubdirectoryPath + "\\" + this.Name + "-" + this.Repository.GetLatestVersion().ToString();
            this.Repository.DownloadVersion( this.Repository.GetLatestVersion(), extractedDirPath );

            // Migrate config files to new version
            DotcmsPlugin.MigrateConfigFiles( this.Path, extractedDirPath );

            // Archive old version
            this.ArchiveCurrentVersion();

            // Move extracted version
            Directory.Move( extractedDirPath, this.Path );
        }

        /// <summary>
        /// Copies .properties configuration files from target directory to destination directory.</summary>
        /// <param name="sourcePluginDir">The target directory from which all .properties files will be copied.</param>
        /// <param name="destPluginDir">The destination directory to which all .properties files will be copied.</param>
        public static void MigrateConfigFiles (string sourcePluginDir, string destPluginDir)
        {
            // TODO: Check that both are plugins
            // TODO: Check that both are the same plugin

            // Copy properties files
            List<string> propertiesFiles = Directory.GetFiles( sourcePluginDir, "*.properties" ).ToList();
            foreach ( string propertiesFilePath in propertiesFiles )
            {
                string propertiesFileName = System.IO.Path.GetFileName( propertiesFilePath );
                File.Copy( propertiesFilePath , destPluginDir + "/" + propertiesFileName );
            }
        }

        /// <summary>
        /// Sends the currently installed version's files to a .zip archive in the plugin-archives subdirectory and deletes it from the plugins subdirectory.</summary>
        private void ArchiveCurrentVersion ()
        {
            // Create archive directory
            string pluginArchiveDirectoryFullPath = this.Dotcms.Path + "/" + DotcmsInstance.PluginArchivesSubdirectory;
            if ( ! Directory.Exists( pluginArchiveDirectoryFullPath ) )
            {
                Directory.CreateDirectory( pluginArchiveDirectoryFullPath );
            }
            
            // Create archive of current plugin
            string currentPluginArchivePath = this.Dotcms.Path + "/" + DotcmsInstance.PluginArchivesSubdirectory + "/" + this.Name + "-" + this.GetInstalledVersion().ToString() + ".zip";
            if ( File.Exists( currentPluginArchivePath ) )
            {
                File.Delete( currentPluginArchivePath );
            }
            ZipFile.CreateFromDirectory( this.Path, currentPluginArchivePath );

            // Clean up unarchived directory
            Directory.Delete( this.Path, true );
        }

    }
}
