dotcms-utils is a collection of PowerShell scripts used for common dotCMS management tasks.

## Usage

### UpdatePlugins.ps1
UpdatePlugins.ps1 updates all plugins defined in the given dotCMS instance's "plugins/plugins.xml" file to the latest version.
#### Syntax
```
UpdatePlugins.ps1 -dotCmsDir "<dotCmsDirPath>"
```

#### Examples
```
UpdatePlugins.ps1 -dotCmsDir "E:\dotCMS\dotcms_3.3.2"
```

### RedeployPlugins.ps1
RedeployPlugins.ps1 freshly deploys a dotCMS instance's plugins and regenerates its service wrapper.
#### Syntax
```
RedeployPlugins.ps1 -dotCmsDir "<dotCmsDirPath>" -yajswDir "<yajswDirPath>"
```
#### Options
-dotCmsDir      Path to the root directory of the dotCMS instance whose plugins are to be updated.
-yajswDir       Path to the root directory of [YAJSW](http://yajsw.sourceforge.net/).

#### Examples
```
E:\bin\dotcms-utils\RedeployPlugins.ps1 -dotCmsDir "E:\dotCMS\dotcms_3.3.2" -yajswDir "C:\yajsw"
```

### GenerateServiceWrapper.ps1
GenerateServiceWrapper.ps1 configures the Windows service wrapper, YAJSW, around a dotCMS instance.

#### Syntax
```
GenerateServiceWrapper.ps1 -dotCmsDir "<dotCmsDirPath>" -yajswDir "<yajswDirPath>"
```

#### Options
-dotCmsDir      Path to the root directory of the dotCMS instance for which a service wrapper is to be generated.
-yajswDir       Path to the root directory of [YAJSW](http://yajsw.sourceforge.net/).

#### Examples
```
E:\bin\dotcms-utils\GenerateServiceWrapper.ps1 -dotCmsDir "E:\dotCMS\dotcms_3.3" -yajswDir "C:\yajsw"
```

### ThreadDump.ps1
ThreadDump.ps1 generates a thread dump for all currently running JVM instances. This script may be useful in debugging hung dotCMS processes.

#### Syntax
```
ThreadDump.ps1 [-outfile "<outfile>"]
```

#### Options
-outfile    Path to the file to which dump output should be written. If not specified, the thread dump is written to stdout.

#### Examples
```
.\ThreadDump.ps1 -outfile "C:\Users\admin-slenk\Desktop\dotCMS Thread Dump $(Get-Date -f 'yyyy-MM-dd HH_mm_ss')"
```


## Testing
This library uses [Pester](https://github.com/pester/Pester) for unit tests. In order to execute the existing tests, [install Pester](http://www.powershellmagazine.com/2014/03/12/get-started-with-pester-powershell-unit-testing-framework/) and run the cmdlet ```Invoke-Pester``` inside this directory.
