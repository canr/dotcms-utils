﻿<#
.SYNOPSIS
Uses the Java service wrapper YAJSW to generate a dotCMS Windows service.

.NOTES
Will require admin privileges on most systems.

.PARAMETER dotCmsDir
Path to the root directory of the dotCMS instance to be wrapped in the service wrapper.

.PARAMETER yajswDir
Path to the root directory of the YAJSW service wrapper.

.EXAMPLE
GenerateServiceWrapper -dotCmsDir "E:\dotCMS\dotcms_3.3" -yajswDir "E:\yajsw"
#>
Param(
    [Parameter(Mandatory=$true, Position=1)] [string] $dotCmsDir,
    [Parameter(Mandatory=$true, Position=2)] [string] $yajswDir
)

# Includes
$here = Split-Path -parent $MyInvocation.MyCommand.Definition
. "$here\Common.ps1"

# Admin check
CheckContinueIfNotAdmin

# Save current directory
$initPwd = pwd

# Shut down dotCMS service if it exists
$serviceWasRunningOnScriptStartup = $false
If (Get-Service dotCMS -ErrorAction SilentlyContinue) {
    $dotCmsServiceStatus = Get-Service dotCMS | Select -ExpandProperty 'Status'
    If ("$dotCmsServiceStatus" -ne 'Stopped') {
        $serviceWasRunningOnScriptStartup = $true
        echo 'Stopping dotCMS service.'
        Stop-Service dotCMS
    }
}

# Start dotCMS by script
cd "$dotCmsDir"
Start-Process -NoNewWindow .\bin\startup.bat
If ($psISE)
{
    Add-Type -AssemblyName System.Windows.Forms
    [System.Windows.Forms.MessageBox]::Show("Click 'OK' when the Tomcat window says `"Server startup in`"")
}
Else
{
    echo 'Press Enter when the Tomcat window says "Server startup in"...'
    $garbage = $host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown')
}

# Get dotCMS process ID
echo 'Determining dotCMS process ID.'
$dotCmsPid = ( Netstat -aon `
                | FindStr '0.0.0.0:80' `
                | %{ $_ -replace '  TCP    0.0.0.0:80             0.0.0.0:0              LISTENING       ', '' })
echo "dotCMS PID: $dotCmsPid"

# Generate service wrapper config
echo 'Generating service wrapper configuration.'
cd "$yajswDir"
java -Xmx30m -Djna_tmpdir="$yajswDir\tmp" -jar "$yajswDir\wrapper.jar" -g "$dotCmsPid" -d "$yajswDir\conf\wrapper.conf.default" "$yajswDir\conf\wrapper.conf"

# Modify service wrapper config
echo 'Modifying service wrapper configuration.'
(Get-Content "$yajswDir\conf\wrapper.conf") `
| %{ $_ -replace 'wrapper.console.title=Tomcat', 'wrapper.console.title=dotCMS' } `
| %{ $_ -replace 'wrapper.ntservice.name=Tomcat', 'wrapper.ntservice.name=dotCMS' } `
| %{ $_ -replace 'wrapper.ntservice.displayname=Tomcat', 'wrapper.ntservice.displayname=dotCMS' } `
| %{ $_ -replace 'wrapper.ntservice.description=Tomcat', 'wrapper.ntservice.description=Tomcat application server running the dotCMS application' } `
| Set-Content "$yajswDir\conf\wrapper.conf"

# Shut down dotCMS
echo 'Shutting down dotCMS.'
Stop-Process -Id "$dotCmsPid"

# Install the service
echo 'Installing the dotCMS service.'
cd "$yajswDir"
java -Xmx30m -Djna_tmpdir="$yajswDir\tmp" -jar "$yajswDir\wrapper.jar" -i "$yajswDir\conf\wrapper.conf"
If (Get-Service dotCMS -ErrorAction SilentlyContinue) {
    echo 'dotCMS service installed.'
    cd $initPwd
} Else {
    echo 'Failed to install dotCMS service.'
    cd $initPwd
    exit 1
}

# Restore dotCMS service if it was running
If ($serviceWasRunningOnScriptStartup) {
    echo 'Starting dotCMS service.'
    Start-Service dotCMS

    echo 'Waiting for dotCMS to start...'
    WaitForDotCmsStartup "$dotCmsDir"

    echo 'dotCMS started.'
}

